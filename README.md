﻿# Sakra Model Service

This service manages the CRUD operations on 
- Factory model (Fusion factory only for now)
- User customizations on the factory model (including layers)

# TODOs
- Finesse work is pending (exception handling, consistent return codes, basic validations have to be done)
- Fine merge of factory and tenant nodes in case of overlap	


# Pre-requisites	

- Build faw-schema, faw-control-plane from master
- Bring up mysql docker
- Bring up faw-control-plane (so that the schema changes take effect)
- Bring up faw-model-service with *java -jar faw-model-service/target/sakra-model-service-0.0.1-SNAPSHOT.jar* (it starts on port 8083)
- Run these bootstrap sqls against the DP schema (this is makeshift in absence of end-to-end wiring)

		- insert into SAKRA_INSTANCE (SAKRA_INSTANCE_ID, SAKRA_TENANT_ID, SAKRA_ACCOUNT_ID, SAKRA_INSTANCE_NAME, SAKRA_INSTANCE_URL,  SAKRA_INSTANCE_EMAIL, SAKRA_INSTANCE_VERSION, SAKRA_INSTANCE_TYPE, SAKRA_INSTANCE_STATE, CREATED_AT, UPDATED_AT)  values ('firsttenant', 'firsttenant', 'firsttenantacc', 'first tenant', 'http://', 'abc@gmail', '1.0.0', 'DEV', 'SUCCESS', current_timestamp, current_timestamp);
		- insert into INSTANCE_LAYER (SAKRA_INSTANCE_ID, LAYER_ORDER, CREATED_AT, CREATED_BY)   values ('firsttenant', 0, current_timestamp, 'user');

## Postman flows
The postman files can be found [here](src/test/resources/FusionModelCRUD.postman_collection.json)