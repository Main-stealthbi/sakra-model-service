package com.sakra.bi.model.svc.model.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity(name = "FUSION_VO_COLS")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class FusionVOColEntity extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "VO_COL_NAME", nullable = false)
    private String name;

    @Column(name = "FUSION_VO_ID", nullable = false)
    private Long fusionVOId;

    @Column(name = "INSTANCE_LAYER_ID")
    private Long instanceLayerId;

    @Column(name = "VO_COL_PROPERTIES", nullable = false, columnDefinition="TEXT")
    private String properties;

}
