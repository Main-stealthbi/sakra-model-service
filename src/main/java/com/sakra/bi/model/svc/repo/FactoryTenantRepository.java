package com.sakra.bi.model.svc.repo;

import com.sakra.bi.model.svc.model.entity.FactoryTenantEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FactoryTenantRepository extends JpaRepository<FactoryTenantEntity, Long> {
}
