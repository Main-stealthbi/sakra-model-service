package com.sakra.bi.model.svc.repo;

import com.sakra.bi.model.svc.model.entity.SuiteModuleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuiteModuleRepository extends JpaRepository<SuiteModuleEntity, Long> {

}