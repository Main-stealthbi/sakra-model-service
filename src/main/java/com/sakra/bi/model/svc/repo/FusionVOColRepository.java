package com.sakra.bi.model.svc.repo;

import com.sakra.bi.model.svc.model.entity.FusionVOColEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FusionVOColRepository extends JpaRepository<FusionVOColEntity, Long> {

}