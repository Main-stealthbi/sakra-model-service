package com.sakra.bi.model.svc.repo;

public interface FusionNodeProperties extends Customizable {
    String nodeId();

    String getNodeName();

    String getNodeProperties();
}
