package com.sakra.bi.model.svc.repo;

import com.sakra.bi.model.svc.model.results.FusionVOColDetail;
import com.sakra.bi.model.svc.model.entity.FusionVOEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FusionVORepository extends JpaRepository<FusionVOEntity, Long> {

    @Query(nativeQuery = true, value = "select FACTORY_ID from INSTANCE_FACTORY_MAPPING where SAKRA_INSTANCE_ID = ?")
    Long getFactoryIdForTenant(String instanceId);

    @Query(nativeQuery = true, value = "select fv.VO_NAME as 'voName', fvc.VO_COL_NAME as 'voColName', " +
            "fvc.VO_COL_PROPERTIES as 'voColProperties' " +
            "from FUSION_VO fv, FUSION_VO_COLS fvc, FACTORY f where fv.FACTORY_ID = f.ID " +
            "and fvc.FUSION_VO_ID = fv.id and f.version = ? and f.suite = ?")
    List<FusionVOColDetail> getAllFusionVOs(String version, String suite);

    @Query(nativeQuery = true, value = "select fv.ID as nodeId, fv.VO_NAME as nodeName, fv.VO_PROPERTIES as nodeProperties from FUSION_VO fv, INSTANCE_LAYER il " +
            "where fv.INSTANCE_LAYER_ID = il.ID and il.SAKRA_INSTANCE_ID = ?")
    List<FusionNodeProperties> getTenantVOs(String instanceId);

    @Query(nativeQuery = true, value = "select fv.ID as nodeId, fv.VO_NAME as nodeName, fv.VO_PROPERTIES as nodeProperties from" +
            " FUSION_VO fv where (FACTORY_ID = :factoryId or INSTANCE_LAYER_ID in (:layers)) and fv.MODULE_NAME = :moduleName" +
            " ORDER BY fv.VO_NAME ASC")
    List<FusionNodeProperties> getTenantVOsForModule(@Param("factoryId") Long factoryId, @Param("layers") List<Long> layers,
                                                     @Param("moduleName") String moduleName);

    @Query(nativeQuery = true, value = "select fvc.ID as nodeId, fvc.VO_COL_NAME as nodeName, fvc.VO_COL_PROPERTIES as nodeProperties " +
            "from FUSION_VO_COLS fvc, FUSION_VO fv where fvc.FUSION_VO_ID = fv.ID and fv.VO_NAME = :voName and (fv.FACTORY_ID = :factoryId or " +
            "fv.INSTANCE_LAYER_ID in (:layers)) ORDER BY fvc.VO_COL_NAME ASC")
    List<FusionNodeProperties> getColumnsForTenantVo(@Param("voName") String voName, @Param("factoryId") Long factoryId, @Param("layers") List<Long> layers);

    @Modifying
    @Query(nativeQuery = true, value = "delete from FUSION_VO where INSTANCE_LAYER_ID in (select ID from INSTANCE_LAYER where SAKRA_INSTANCE_ID = ?)")
    void deleteTenantVos(String tenantId);

}