package com.sakra.bi.model.svc.model.entity;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity(name = "SUITE_MODULE")
@SuperBuilder
public class SuiteModuleEntity extends AuditEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "SUITE", nullable = false)
    private String suite;

    @Column(name = "NAME", nullable = false)
    private String name;

}
