package com.sakra.bi.model.svc.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sakra.bi.model.svc.exception.SakraModelException;
import com.sakra.bi.model.svc.model.results.FusionVOColDetail;
import com.sakra.bi.model.svc.model.FusionVORequest;
import com.sakra.bi.model.svc.model.Suite;
import com.sakra.bi.model.svc.model.entity.*;
import com.sakra.bi.model.svc.repo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.sakra.bi.model.svc.utils.JsonUtils.mapper;
import static com.sakra.bi.model.svc.utils.JsonUtils.safeGetJson;

@Service
@Slf4j
public class FusionVOService {

    private final String fusion = Suite.fusion.name();

    private final String defaultLayer = "DEFAULT";

    @Autowired
    private FactoryRepository factoryRepository;

    @Autowired
    private FusionVORepository fusionVORepository;

    @Autowired
    private FusionVOColRepository fusionVOColRepository;

    @Autowired
    private SuiteModuleRepository suiteModuleRepository;

    @Autowired
    private InstanceLayerRepository instanceLayerRepository;

    @Autowired
    private FactoryTenantRepository factoryTenantRepository;

    @Autowired
    private FusionExtractor fusionExtractor;

    public JsonNode getFactory(String version) {
        Optional<FactoryEntity> factoryOpt = factoryRepository.findOne(Example.of(FactoryEntity.builder().
                suite(fusion).version(version).createdAt(null).isLatest(null).build()));
        if (!factoryOpt.isPresent()) {
            return null;
        }

        List<FusionVOColDetail> fusionVOEntities = fusionVORepository.getAllFusionVOs(version, fusion);
        ObjectNode results = JsonNodeFactory.instance.objectNode();
        fusionVOEntities.stream().collect(Collectors.groupingBy(FusionVOColDetail::getVoName)).
                forEach((s, fusionVODetails) -> results.set(s, fusionVODetails.stream().
                        map(fusionVODetail -> safeGetJson(fusionVODetail.getVoColProperties())).
                        collect(mapper::createArrayNode, ArrayNode::add,
                                (jsonNodes, jsonNodes2) -> jsonNodes.addAll(jsonNodes2))));
        return results;
    }

    public JsonNode getTenantVosForModule(String tenantId, String moduleName){
        List<Long> layers = instanceLayerRepository.getInstanceLayers(tenantId, instanceLayerRepository.getLayerOrder(tenantId, defaultLayer));
        Long factoryId = fusionVORepository.getFactoryIdForTenant(tenantId);
        List<FusionNodeProperties> allVOs = fusionVORepository.getTenantVOsForModule(factoryId, layers, moduleName);
        ObjectNode results = JsonNodeFactory.instance.objectNode();
        ArrayNode arrayNode = allVOs.stream().map(fn -> safeGetJson(fn.getNodeProperties())).
                collect(mapper::createArrayNode, ArrayNode::add, (jsonNodes, jsonNodes2) -> jsonNodes.addAll(jsonNodes2));
        results.set("list", arrayNode);
        return results;
    }

    public JsonNode getColumnsForTenantVO(String tenantId, String voName){
        List<Long> layers = instanceLayerRepository.getInstanceLayers(tenantId, instanceLayerRepository.getLayerOrder(tenantId, defaultLayer));
        Long factoryId = fusionVORepository.getFactoryIdForTenant(tenantId);
        List<FusionNodeProperties> allVOs = fusionVORepository.getColumnsForTenantVo(voName, factoryId, layers);
        ObjectNode results = JsonNodeFactory.instance.objectNode();
        ArrayNode arrayNode = allVOs.stream().map(fn -> safeGetJson(fn.getNodeProperties())).
                collect(mapper::createArrayNode, ArrayNode::add, (jsonNodes, jsonNodes2) -> jsonNodes.addAll(jsonNodes2));
        results.set("list", arrayNode);
        return results;
    }

    @Transactional
    public Long saveFusionFactory(FusionVORequest factReq) throws SakraModelException {
        Optional<FactoryEntity> optFactory = factoryRepository.findOne(Example.of(FactoryEntity.builder().
                suite(fusion).version(factReq.getVersion()).createdAt(null).isLatest(null).build()));
        FactoryEntity factoryEntity = optFactory.isPresent() ? optFactory.get() : null;
        if (factoryEntity == null) {
            factoryEntity = factoryRepository.save(FactoryEntity.builder().suite(fusion).
                    version(factReq.getVersion()).createdBy(factReq.getRequestor()).build());
            //throw new SakraModelException("Factory " + optFactory.get() + " exists");
        }

        JsonNode allVos = factReq.getJsonVOs().get("voList");
        for (JsonNode jsonNode : allVos) {
            String voName = jsonNode.get("name").asText();
            log.info("Saving vo " + voName);
            FusionVOEntity fusionVOEntity = fusionExtractor.extractVOEntity(factReq, jsonNode, factoryEntity.getId(), null);
            fusionVOEntity = fusionVORepository.save(fusionVOEntity);

            JsonNode columns = jsonNode.get("columns");
            fusionVOColRepository.saveAll(fusionExtractor.extractVOCols(fusionVOEntity, columns, null));
        }

        return -1l;
    }

    @Transactional
    public void deleteFusionFactory(FusionVORequest factReq) throws SakraModelException {
        Optional<FactoryEntity> factoryEntity = factoryRepository.findOne(Example.of(FactoryEntity.builder().
                suite(fusion).version(factReq.getVersion()).createdAt(null).isLatest(null).build()));
        if (!factoryEntity.isPresent()) {
            log.info("Skip deletion - couldn't find factory with version "+factReq.getVersion());
            return;
        }

        factoryRepository.deleteById(factoryEntity.get().getId());
    }

    @Transactional
    public void deleteTenantExtensions(String tenantId) throws SakraModelException {
        fusionVORepository.deleteTenantVos(tenantId);
    }

    public List<String> getModules(String version) {
        List<SuiteModuleEntity> allSuiteModules = suiteModuleRepository.findAll(Example.of(SuiteModuleEntity.builder().
                suite(Suite.fusion.name()).build()));
        return allSuiteModules.stream().map(SuiteModuleEntity::getName).collect(Collectors.toList());
    }

    @Transactional
    public Long saveTenantVOs(FusionVORequest fusionVORequest) throws SakraModelException {
        Optional<InstanceLayerEntity> instanceLayerEntity = instanceLayerRepository.findOne(Example.of(
                InstanceLayerEntity.builder().tenantId(fusionVORequest.getTenantId()).
                        layer(fusionVORequest.getLayer()).layerOrder(null).createdAt(null).build()));
        if (!instanceLayerEntity.isPresent()) {
            return -1L; //TODO: throw exception
        }

        JsonNode allVos = fusionVORequest.getJsonVOs().get("voList");
        for (JsonNode jsonNode : allVos) {
            String voName = jsonNode.get("name").asText();
            log.info("Processing vo {} ", voName);
            Long layerId = instanceLayerEntity.get().getId();
            Optional<FusionVOEntity> foundEntity = fusionVORepository.findOne(Example.of(FusionVOEntity.builder().
                    name(voName).moduleName(fusionVORequest.getModule()).instanceLayerId(layerId).build()));
            FusionVOEntity fusionVOEntity = fusionExtractor.extractVOEntity(fusionVORequest, jsonNode, null, layerId);
            if (foundEntity.isPresent()) {
                log.info("Updating VO {}", voName);
                foundEntity.get().setProperties(fusionVOEntity.getProperties());
                fusionVOEntity = foundEntity.get();
            }

            fusionVOEntity = fusionVORepository.save(fusionVOEntity);
            JsonNode columns = jsonNode.get("columns");
            List<FusionVOColEntity> cols = fusionExtractor.extractVOCols(fusionVOEntity, columns,
                    instanceLayerEntity.get().getId());
            for (FusionVOColEntity col : cols) {
                log.info("Processing vo column {} ", col.getName());
                Optional<FusionVOColEntity> foundColEnt = fusionVOColRepository.findOne(Example.of(FusionVOColEntity.builder().
                        name(col.getName()).instanceLayerId(layerId).build()));
                if (foundColEnt.isPresent()) {
                    log.info("Updating VO Col {}", voName);
                    foundColEnt.get().setProperties(col.getProperties());
                    col = foundColEnt.get();
                }
                fusionVOColRepository.save(col);
            }
        }

        return -1L;
    }

    @Transactional
    public void registerFactoryAgainstTenant(String tenantId, String version) {
        Optional<FactoryEntity> optFactory = factoryRepository.findOne(Example.of(FactoryEntity.builder().
                suite(fusion).version(version).createdAt(null).isLatest(null).build()));
        if(!optFactory.isPresent()) {
            throw new NotFoundException("Could not find factory with version "+version);
        }

        factoryTenantRepository.save(FactoryTenantEntity.builder().
                factoryTenantKey(new FactoryTenantEntity.FactoryTenantKey(tenantId, optFactory.get().getId())).
                createdBy("system").build());
    }

}
