package com.sakra.bi.model.svc.model.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity(name = "FUSION_VO")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class FusionVOEntity extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "VO_NAME", nullable = false)
    private String name;

    @Column(name = "INSTANCE_LAYER_ID", nullable = false)
    private Long instanceLayerId;

    @Column(name = "FACTORY_ID", nullable = false)
    private Long factoryId;

    @Column(name = "EXECUTION_ORDER")
    private int executionOrder;

    @Column(name = "MODULE_NAME")
    private String moduleName;

    @Column(name = "VO_PROPERTIES", nullable = false, columnDefinition="TEXT")
    private String properties;

}
