package com.sakra.bi.model.svc.model.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name="INSTANCE_FACTORY_MAPPING")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class FactoryTenantEntity extends AuditEntity{

    @EmbeddedId
    private FactoryTenantKey factoryTenantKey;

    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    public static class FactoryTenantKey implements Serializable {
        @Column(name="SAKRA_INSTANCE_ID", nullable = false)
        private String instanceId;

        @Column(name = "FACTORY_ID", nullable = false)
        private Long factoryId;
    }

}
