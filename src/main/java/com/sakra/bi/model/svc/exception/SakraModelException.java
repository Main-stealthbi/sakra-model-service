package com.sakra.bi.model.svc.exception;

public class SakraModelException extends Exception {

    public SakraModelException(String message) {
        super(message);
    }
}
