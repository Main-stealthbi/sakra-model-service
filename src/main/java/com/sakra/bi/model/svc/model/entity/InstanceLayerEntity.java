package com.sakra.bi.model.svc.model.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity(name = "INSTANCE_LAYER")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class InstanceLayerEntity extends AuditEntity
{

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "SAKRA_INSTANCE_ID", nullable = false)
    private String tenantId;

    @Column(name = "LAYER", nullable = false)
    @Builder.Default
    private String layer = "DEFAULT";

    @Column(name = "LAYER_ORDER")
    @Builder.Default
    private Integer layerOrder = 0;

}
