package com.sakra.bi.model.svc.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sakra.bi.model.svc.model.entity.FusionVOColEntity;
import com.sakra.bi.model.svc.model.entity.FusionVOEntity;
import com.sakra.bi.model.svc.model.FusionVORequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class FusionExtractor {

    private Set<String> fieldsToSkip = new HashSet<>(Arrays.asList("columns"));

    public FusionVOEntity extractVOEntity(FusionVORequest factReq, JsonNode jsonNode, Long factoryId, Long instanceLayerId) {
        ObjectNode newNode = JsonNodeFactory.instance.objectNode();
        jsonNode.fields().forEachRemaining(stringJsonNodeEntry -> {
            if (!fieldsToSkip.contains(stringJsonNodeEntry.getKey())) {
                newNode.set(stringJsonNodeEntry.getKey(), stringJsonNodeEntry.getValue());
            }
        });

        FusionVOEntity fusionVO = FusionVOEntity.builder().
                factoryId(factoryId).instanceLayerId(instanceLayerId).name(jsonNode.get("name").asText()).executionOrder(0).
                moduleName(factReq.getModule()).properties(newNode.toString()).createdBy(factReq.getRequestor()).build();
        return fusionVO;
    }

    public List<FusionVOColEntity> extractVOCols(FusionVOEntity fusionVOEntity, JsonNode jsonNode, Long instanceLayerId) {
        if (!jsonNode.isArray()) {
            log.error("columns is expected to be an array, instead got " + jsonNode);
            return Collections.emptyList();
        }

        List<FusionVOColEntity> columns = new ArrayList<>();
        for (JsonNode colJson : jsonNode) {
            ObjectNode newColNode = JsonNodeFactory.instance.objectNode();
            colJson.fields().forEachRemaining(colJsonEntry -> {
                if (!fieldsToSkip.contains(colJsonEntry.getKey())) {
                    newColNode.set(colJsonEntry.getKey(), colJsonEntry.getValue());
                }
            });
            columns.add(FusionVOColEntity.builder().fusionVOId(fusionVOEntity.getId()).
                    name(colJson.get("name").asText()).properties(newColNode.toString()).
                    createdBy(fusionVOEntity.getCreatedBy()).build());
        }

        return columns;
    }
}
