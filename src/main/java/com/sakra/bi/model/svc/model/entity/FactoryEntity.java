package com.sakra.bi.model.svc.model.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity(name = "FACTORY")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class FactoryEntity extends AuditEntity
{

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "SUITE", nullable = false)
    private String suite;

    @Column(name = "VERSION", nullable = false)
    private String version;

    @Column(name = "IS_LATEST")
    @Builder.Default
    private Boolean isLatest = false;

}
