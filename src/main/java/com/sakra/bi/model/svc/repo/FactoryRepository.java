package com.sakra.bi.model.svc.repo;

import com.sakra.bi.model.svc.model.entity.FactoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface FactoryRepository extends JpaRepository<FactoryEntity, Long> {

    @Modifying
    @Query(nativeQuery = true, value = "insert into INSTANCE_FACTORY_MAPPING values (?, ?, current_timestamp, current_timestamp")
    void registerTenantAgainstFactory(String tenantId, Long factoryId);

}