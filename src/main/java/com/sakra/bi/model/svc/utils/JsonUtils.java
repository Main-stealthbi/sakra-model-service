package com.sakra.bi.model.svc.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.List;

public final class JsonUtils {

    public static final ObjectMapper mapper = new ObjectMapper();

    public static <T> JsonNode toJson(List<T> inputList) {
        ArrayNode newNode = JsonNodeFactory.instance.arrayNode();
        for(T t : inputList) {
            newNode.add(t.toString());
        }

        return newNode;
    }

    public static String safeString(JsonNode jsonNode, String element) {
        try {
            return jsonNode.get(element).asText();
        } catch (Exception ex) {
        }

        return null;
    }

    public static JsonNode safeGetJson(String value) {
        try {
            return mapper.readTree(value);
        } catch (Exception ex) {
        }

        return null;
    }
}
