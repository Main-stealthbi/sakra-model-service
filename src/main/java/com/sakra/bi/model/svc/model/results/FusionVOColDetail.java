package com.sakra.bi.model.svc.model.results;

public interface FusionVOColDetail {

    String getVoName();

    String getVoColName();

    String getVoColProperties();

}
