package com.sakra.bi.model.svc.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
public class FusionVORequest {

    @NotEmpty
    @Size(max = 80)
    private String version;

    private String tenantId;

    private String layer;

    private String module;

    @NotEmpty
    private JsonNode jsonVOs;

    private String requestor;

}
