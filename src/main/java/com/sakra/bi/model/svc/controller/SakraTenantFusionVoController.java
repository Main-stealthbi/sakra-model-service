package com.sakra.bi.model.svc.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.sakra.bi.model.svc.exception.SakraModelException;
import com.sakra.bi.model.svc.model.FusionVORequest;
import com.sakra.bi.model.svc.service.FusionVOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;

@RestController
    @RequestMapping("/api/modelservice/tenant/{tenantId}/fusion")
public class SakraTenantFusionVoController {

    @Autowired
    private FusionVOService fusionVOService;

    @PostMapping("/{version}")
    public ResponseEntity<Void> registerTenantWithFactory(@PathVariable("tenantId") String tenantId,
                                                    @PathVariable("version") String version) {
        fusionVOService.registerFactoryAgainstTenant(tenantId, version);
        return new ResponseEntity<Void>(null, null, HttpStatus.CREATED);
    }

    @GetMapping("/modules/{moduleName}/vos")
    public ResponseEntity<JsonNode> getVOsForModule(@PathVariable("tenantId") String tenantId,
                                                @PathVariable("moduleName") String moduleName) {
        JsonNode results = fusionVOService.getTenantVosForModule(tenantId, moduleName);
        return new ResponseEntity<JsonNode>(results, null, HttpStatus.OK);
    }

    @GetMapping("/modules/{moduleName}/vos/{voName}/columns")
    public ResponseEntity<JsonNode> getColumnsForVO(@PathVariable("tenantId") String tenantId,
                                                    @PathVariable("voName") String voName) {
        JsonNode results = fusionVOService.getColumnsForTenantVO(tenantId, voName);
        return new ResponseEntity<JsonNode>(results, null, HttpStatus.OK);
    }

    @PostMapping("/modules/{moduleName}/vos")
    public ResponseEntity<Void> saveTenantVOs(@PathVariable("tenantId") String tenantId,
                                              @PathVariable("moduleName") String module,
                                              @Valid @RequestBody JsonNode jsonNode) throws SakraModelException {
        fusionVOService.saveTenantVOs(FusionVORequest.builder().tenantId(tenantId).module(module).jsonVOs(jsonNode).requestor("user").build());
        return new ResponseEntity<Void>(null, null, HttpStatus.CREATED);
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteVo(@PathVariable("tenantId") String tenantId) throws SakraModelException {
        fusionVOService.deleteTenantExtensions(tenantId);
        return new ResponseEntity<Void>(null, null, HttpStatus.OK);
    }
}
