package com.sakra.bi.model.svc.repo;

public interface Customizable {

    Boolean isCustomized();
}
