package com.sakra.bi.model.svc.repo;

import com.sakra.bi.model.svc.model.entity.InstanceLayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface InstanceLayerRepository extends JpaRepository<InstanceLayerEntity, Long> {

    @Query(nativeQuery = true, value = "select ID from INSTANCE_LAYER il where SAKRA_INSTANCE_ID = ? and " +
            "LAYER_ORDER <= ? ")
    List<Long> getInstanceLayers(String tenantId, Integer layerOrder);

    @Query(nativeQuery = true, value = "select LAYER_ORDER from INSTANCE_LAYER where SAKRA_INSTANCE_ID = ? and LAYER = ?")
    Integer getLayerOrder(String tenantId, String layer);

}