package com.sakra.bi.model.svc.model.results;

public interface FusionVO {
    public Long id();

    public String voName();

    public String voProperties();
}
