package com.sakra.bi.model.svc.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.sakra.bi.model.svc.model.FusionVORequest;
import com.sakra.bi.model.svc.model.Suite;
import com.sakra.bi.model.svc.service.FusionVOService;
import com.sakra.bi.model.svc.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/modelservice/factory")
public class SakraFactoryFusionVoController {

    @Autowired
    private FusionVOService fusionVOService;

    @PostMapping("/fusion/{version}/{module}")
    public ResponseEntity<Void> registerFactory(@PathVariable("version") String version,
                                                @PathVariable("module") String module,
                                                @Valid @RequestBody JsonNode jsonNode,
                                                UriComponentsBuilder uriComponentsBuilder) throws Exception {
        fusionVOService.saveFusionFactory(FusionVORequest.builder().version(version).module(module).jsonVOs(jsonNode).requestor("system").build());
        UriComponents uriComponents = uriComponentsBuilder.path("/api/modelservice/factory/fusion/{version}/{module}").buildAndExpand(version, module);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/fusion/{version}")
    public ResponseEntity<Void> deleteFactory(@PathVariable("version") String version,
                                                  UriComponentsBuilder uriComponentsBuilder) throws Exception {
        fusionVOService.deleteFusionFactory(FusionVORequest.builder().version(version).requestor("system").build());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/fusion/{version}/modules")
    public ResponseEntity<JsonNode> getModules(@PathVariable("suite") String suite,
                                                  @PathVariable("version") String version,
                                                  UriComponentsBuilder uriComponentsBuilder) throws Exception  {
        List<String> modules = fusionVOService.getModules(suite);
        return new ResponseEntity<JsonNode>(JsonUtils.toJson(modules), null, HttpStatus.OK);
    }

    @GetMapping("/fusion/{version}/vos")
    public ResponseEntity<JsonNode> getVOs(@PathVariable("version") String version) throws Exception {
        JsonNode results = fusionVOService.getFactory(version);
        return new ResponseEntity<JsonNode>(results, null, HttpStatus.OK);
    }
}
